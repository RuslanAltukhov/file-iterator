<?php
require_once "PolyLine.php";
define("API_KEY", "AIzaSyC8UUQSR3gS0i9ELtpdpqepfOzyYNhq5h8");
/**
 * Отправка запроса в directions api
 * @param float $from_lat
 * @param float $from_lng
 * @param float $to_lat
 * @param float $to_lng
 */
function make_request(float $from_lat, float $from_lng, float $to_lat, float $to_lng)
{
//https://api.mapbox.com/directions/v5/mapbox/driving/-122.42,37.78;-77.03,38.91?access_token=pk.eyJ1IjoicHJvZ2VyMTUwIiwiYSI6ImNpbGU4Zm5yZDAwMXd2a2txMmZzOWpod2sifQ.ia7VdlsxhFj3fO1-x26MeQ&geometries=geojson
    $key = "AIzaSyC8UUQSR3gS0i9ELtpdpqepfOzyYNhq5h8";
    $params = [
        'origin' => "{$from_lat},{$from_lng}",
        'destination' => "{$to_lat},{$to_lng}",
        'mode'=>'driving',
        'key' => API_KEY
    ];
    $curl = curl_init("https://maps.googleapis.com/maps/api/directions/json?" . http_build_query($params));
   // echo "https://maps.googleapis.com/maps/api/directions/json?" . http_build_query($params);
  //  die;
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    return json_decode($data);
}

/**
 * Перевод ответа от directions api в массив точек
 */
function decode_route_to_points(stdClass $data)
{
    $points = [];
    $string = "";
    $legs = (($data->routes[0] ?? [])->legs ?? []);
    foreach ($legs as $leg) {
        foreach ($leg->steps as $step) {

                $points[] = ['x' => $step->start_location->lat, 'y' => $step->start_location->lng];
               $points += PolylineEncoder::decodeValue($step->polyline->points);
                $points[] = ['x' => $step->end_location->lat, 'y' => $step->end_location->lng];

        }

    }

    return $points;
}

$points = decode_route_to_points(make_request(56.1917, 43.9950943, 55.73020336, 37.62405396));

?>
<html>
<head>

</head>
<body>
<div style="width:100%;height:500px" id="map">

</div>
<script>
    var points = <?=json_encode($points);?>;
    var markers = [];
    var map;
console.log(points.length);
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 56.2804, lng: 44.0254},
            zoom: 8
        });
        for(i = 0;i < points.length;i ++)
        {

              addMarker(points[i]);

        }

    }

    function addMarker(latlng) {

        return new google.maps.Marker({
            position: {lat:latlng.x,lng:latlng.y},
            map: map,
            title:latlng.x + ',' + latlng.y
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= API_KEY ?>&callback=initMap"></script>
</body>
</html>

