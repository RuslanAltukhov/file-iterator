<?php
/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 21.12.2017
 * Time: 13:29
 */

class FileIterator implements SeekableIterator
{
    private $handler;
    private $position = 0;
    private $total_size = 0;
    public function __construct($fname)
    {
        $this->handler = fopen($fname,'r');
        $this->total_size = filesize($fname);
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        $char = fgetc($this->handler);
        fseek($this->handler,$this->position);
        return $char;
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        fseek($this->handler, ++$this->position);
    }


    public function key()
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return $this->position <= $this->total_size;
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->position = 0;
        rewind($this->handler);
    }

    /**
     * Seeks to a position
     * @link http://php.net/manual/en/seekableiterator.seek.php
     * @param int $position <p>
     * The position to seek to.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function seek($position)
    {
        fseek($this->handler, $position);
    }
}